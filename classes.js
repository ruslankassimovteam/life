/* ===========================================

	Those classes are used to instantiate  game objects.

=========================================== */
(function(Life) {
	Life.Classes = {};

	// MAP
	var Zone = {
		'Blocking':true,			// is this zone blocking
		'MoveSpeedBuff':0,	// Move speed alternate (+, -)
		'Color':'#000'				// Zone color
	};
	
	Life.Classes.Zone = Zone;

	// FOOD
	var Food = {
		'Name':'Food',			// Food name
		'GenesNutrition':[],		// For each gene, nutrition (+, -)
		'Color':'#000'				// Food color on map
	};
	
	Life.Classes.Food = Food;
	
	// GENES
	var Gene = {
		'Name':'Gene',					// Gene name
		'HP':1,								// HP added
		'Strength':1,						// Strength added
		'Sight':1,							// Sight added
		'MoveSpeed':1,					// MoveSpeed added
		'CollectSpeed':1,				// Food collect speed
		'ReproductionSpeed':1,		// Reproduction timer
		'Aggressivity':1,					// Aggressivity
		'FoodCarry':1,					// How many food can one carry
		'FoodConsumption':1,			// Food consumption timer
	};
	
	Life.Classes.Gene = Gene;
	
	// LIFE FORM
	var LifeForm = {
		'Name':'Life Form',				// Life Form Name
		'Generation':1,					// Life Form Generation number
		'Color':'#000',						// Life form Color
		'Status':'alive',					// Life Form status
	};
	
	Life.Classes.LifeForm = LifeForm;
})(Life);