/* ===========================================

			Classic game loop and AI methods.

=========================================== */
(function(Life) {
	Life.cycle = 0;
	
	Life.Engine = {
			
		'game_loop' : function() {
			Life.Engine.life_ctx.clearRect(0,0,750,750);
			
			var pop = 0;
			var j;
			var tick = new Date();
			for(j=0;j<Life.Map.Lifes.length;++j)
			{
				var life = Life.Map.Lifes[j];
				if(life.status == 'dead') continue;
				
				pop++;
				
				// COLLECT
				if(tick-life.last_collect>=life.data.CollectSpeed) {
					
					var near_food = false;
					for(k=0;k<Life.Map.Food.length;++k)
					{
						if(Life.Map.intersects({'x':life.x-2,'y':life.y-2,'w':5,'h':5},Life.Map.Food[k])) {
							near_food=true;
							break;
						}
					}
					
					if(near_food && Life.Map.Food[k].qt>=0) {
						life.last_collect=tick;
						near_food=k;
						
						for(l=0;l<life.data.Genes.length;++l) {
						
							var collected = Life.Map.Food[k].data.GenesNutrition[life.data.Genes[l].ID];
							
							Life.Map.Food[k].qt-=Math.abs(collected);
						
							if(life.food+collected<=life.data.FoodCarry) {
								if(life.food+collected>=0) {
									life.food+=collected;
									//logs(life.data.Name+' is eating '+Life.Map.Food[k].data.Name+ '('+collected+'=>'+life.food+')',1);
								}
								else {
									logs(life.data.Name+' was poisoned by '+Life.Map.Food[k].data.Name,3);
									life.status = 'dead';
									break;
								}
							}
							else {								
								life.food=life.data.FoodCarry;
								//logs(life.data.Name+' is full of food '+life.food+'/'+life.data.FoodCarry+'  :)',2);
								break;
							}
						}
					}
				}
							
				// EAT
				if(tick-life.last_consumption>=life.data.FoodConsumption) {
					life.last_consumption=tick;
					if(--life.food<=0) {
						logs(life.data.Name+' died from starvation :(',3);
						life.status = 'dead';
						continue;
					}					
				}
				
				// REPRODUCE
				if(tick-life.last_reproduction>=life.data.ReproductionSpeed) {
					
					
					var near_mate = false;
					for(k=0;k<Life.Map.Lifes.length;++k)
					{
						if(Life.Map.intersects({'x':life.x-4,'y':life.y-4,'w':9,'h':9},Life.Map.Lifes[k]) && life.data.Name===Life.Map.Lifes[k].data.Name && Life.Map.Lifes[k]!== life) {
							near_mate=true;
							break;
						}
					}
					if(near_mate)
					{
						near_mate=k;
						life.last_reproduction=tick;
						var new_life = Object.create(life);
						new_life.data.Generation++;
						Life.Map.Lifes.push(new_life);
						logs(life.data.Name+' just got some sexy time ;)',1);
					}
				}
				
				var where = chance.integer({min: 1, max: 4});
				Life.Engine.move(life,where);
				
				Life.Engine.life_ctx.fillStyle=life.data.Color;
				Life.Engine.life_ctx.fillRect(life.x,life.y,life.w,life.h);	
				
			}
			
			// Draw food
			Life.Engine.world_ctx.clearRect(0,0,750,750);
			for(i=0;i<Life.Map.Food.length;++i) {
				var food = Life.Map.Food[i];
				if(food.qt>0) {
					Life.Engine.world_ctx.fillStyle=food.data.Color;
					Life.Engine.world_ctx.fillRect(food.x,food.y,food.w,food.h);
				}
			}
			
			
			// Game timers
			Life.Engine.life_ctx.fillStyle = '#eee';
			Life.Engine.life_ctx.font = 'bold 20px Arial';
			Life.Engine.life_ctx.fillText('Population : '+pop,10 ,30);
			Life.Engine.life_ctx.fillText('Timer : '+parseInt((new Date()-Life.timestamp)/1000),300 ,30);
			Life.Engine.life_ctx.font = '11px Arial';
			Life.Engine.life_ctx.fillText('(game_loop : '+(new Date()-Life.last_tick)+')',430 ,27);	
			Life.Engine.life_ctx.font = 'bold 20px Arial';
			Life.Engine.life_ctx.fillText('Cycle : '+Life.cycle,580 ,30);
			
			Life.last_tick = new Date();
			Life.cycle++;
			
			if(pop>0)
				window.requestAnimationFrame(Life.Engine.game_loop);
			else
				logs('Global extinction !',3);
		},
		
		// LIFE
		'move' : function(life,dir) {
			var intersects= false,j;
			
			if(dir==1 && life.y>0) life.y--;
			else if(dir==2 && life.x<749) life.x++;
			else if(dir==3 && life.y<749) life.y++;
			else if(dir==4 && life.x>0) life.x--;
			else return;
			
			for(j=0;j<Life.Map.Food.length;++j)
			{
				if(Life.Map.intersects(life,Life.Map.Food[j])) {
					intersects=true;
					break;
				}
			}
			
			if(intersects) {
				//logs(life.data.Name+' can\'t move !',2);
				if(dir==1 && life.y>0) life.y++;
				else if(dir==2 && life.x<749) life.x--;
				else if(dir==3 && life.y<749) life.y--;
				else if(dir==4 && life.x>0) life.x++;
				else return;
				return false;
			}
			else {
				for(j=0;j<Life.Map.Zones.length;++j)
				{
					if(Life.Map.intersects(life,Life.Map.Zones[j]) && Life.Map.Zones[j].data.Blocking) {
						intersects=true;
						break;
					}
				}
			}
			if(intersects) {
				//logs(life.data.Name+' can\'t move !',2);
				if(dir==1 && life.y>0) life.y++;
				else if(dir==2 && life.x<749) life.x--;
				else if(dir==3 && life.y<749) life.y--;
				else if(dir==4 && life.x>0) life.x++;
				else return;
				return false;
			}
			else {
				//logs(life.data.Name+' is moving !',1);
				return true;
			}
		}
	}
	
})(Life);