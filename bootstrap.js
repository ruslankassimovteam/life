/* ===========================================

			Game configuration, logs and Yet Another Useless Framework.

=========================================== */
(function(window) {
	
	window.Life = {
		'Conf':{
			//Global
			'LifeFormPoolSize':6,
			'GenesPoolSize':5,
			'FoodPoolSize':5,
			
			//MAP
			'FoodNb':200,
			'ZonesNb':50,
			'LifesNb':120,
			
			//LifeForm
			'GenesPerLifeForm':10,
		},
		'timestamp': new Date()
	};
	
	window.logs = function(msg, lvl) {
		//if($('#logs p').length>1000) $('#logs p:lt(1000)').remove();
		
		$('#logs').append('<p class="'+
			(lvl?
				(lvl==1?'notice':
					(lvl==2?'warning':
						(lvl==3?'alert':'')
					)
				)
			:'')+'">['+YAUF.pad(5,(new Date()-Life.timestamp).toString(),'.')+']'+
			(msg==''?'<br/>':msg).replace(/ /g, '&nbsp;').replace(/\t/g, '&nbsp;&nbsp;&nbsp;&nbsp;')
			+'</p>');
		$("#logs").scrollTop($("#logs")[0].scrollHeight);
	};
	
	window.YAUF = {
		'dump_array':function (arr,level,max_level) {
			var dumped_text = "";
			if(!level) level = 0;

			var level_padding = "";
			for(var j=0;j<level+1;j++) level_padding += "    ";

			if(typeof(arr) == 'object') {  
				for(var item in arr) {
					var value = arr[item];

					if(typeof(value) == 'object' && level<=max_level) { 
						dumped_text += level_padding + "'" + item + "' ...<br/>";
						dumped_text += this.dump_array(value,level+1);
					} else {
						if(item!='Genes')
						dumped_text += level_padding + "'" + item + "' => \"" + value + "\"<br/>";
					}
				}
			} else { 
				dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
			}
			return dumped_text;
		},
		'randomBetween': function (min,max) {
			return Math.random() * (max - min) + min;
		},
		'randomBetweenInt': function(min,max) {
			return Math.round(this.randomBetween(min,max));
		},
		'pad' : function (width, string, padding) { 
		  return (width <= string.length) ? string : YAUF.pad(width, padding + string, padding)
		}
	};
})(window);


$(function() {
	logs('=======================================================================');
	logs('			        #        ###  #######  ####### ');
	logs('			        #         #   #        #       ');
	logs('			 #####  #         #   #        #       ');
	logs('			        #         #   #####    #####   ');
	logs('			 #####  #         #   #        #       ');
	logs('			        #         #   #        #       ');
	logs('			        #######  ###  #        ####### ');
	logs('=======================================================================');
	logs('');
	logs('Bootstraping ....');
	logs('Notice test.',1);
	logs('Warning test.',2);
	logs('Alert test.',3);
	logs('');
	logs('Dumping Conf :',1);
	logs(YAUF.dump_array(Life.Conf,0,1));
	logs('');
	logs('Populating infos...');
	Life.Data.Div.populate();
	logs('');
	Life.Map.generate();
	/*logs('');
	logs('Dumping Life Forms : ',1);
	logs(YAUF.dump_array(Life.Data.LifeForms,0,1));
	logs('');
	logs(YAUF.dump_array(Life.Map.Lifes,0,30));
	logs('');*/
	
	Life.Engine.life_ctx = $('#life').get(0).getContext('2d');
	Life.Engine.world_ctx = $('#world').get(0).getContext('2d');
	
	logs('Launching game !',3);
	Life.last_tick = new Date();
	
	// Game Loop
	window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
	window.requestAnimationFrame(Life.Engine.game_loop());
});