/* ===========================================

			Here game data objects are instantiated.

=========================================== */
(function(Life) {
	Life.Data = {};
	Life.Data.Div = {									// Populate game data Div
		'populate' : function() {
			for(var k in this) {
				if(k!='populate') {
					$('#'+k).append(this[k]);
				}
			}
		},
		'genes' : '<h2>Genes : </h2>',
		'foods' : '<h2>Foods : </h2>',
		'lifeforms' : '<h2>Life forms : </h2>'
	};
	
	// ZONES
	var Water = Object.create(Life.Classes.Zone);
	Water.Blocking=false;
	Water.MoveSpeedBuff=-0.5;
	Water.Color='#1A8E88';
	
	var Forest = Object.create(Life.Classes.Zone);
	Forest.Blocking=true;
	Forest.Color='#043D23';
	
	Life.Data.Zones = [Water,Forest];

	// GENE
	Life.Data.Genes = [];
	var i;
	for(i=0;i<Life.Conf.GenesPoolSize;++i){
		var Gene = Object.create(Life.Classes.Gene);
		Gene.ID = i;
		Gene.Name=chance.word();
		Gene.HP=YAUF.randomBetweenInt(-1,20);
		Gene.Strength=YAUF.randomBetweenInt(-1,3);
		Gene.Sight=YAUF.randomBetweenInt(1,2);
		Gene.MoveSpeed=YAUF.randomBetweenInt(-1,10);
		Gene.CollectSpeed=YAUF.randomBetweenInt(-1,50);
		Gene.ReproductionSpeed=YAUF.randomBetweenInt(-1,10000);
		Gene.Aggressivity=YAUF.randomBetweenInt(-1,3);
		Gene.FoodCarry=YAUF.randomBetweenInt(-1,100);
		Gene.FoodConsumption=YAUF.randomBetweenInt(1,100);
		
		Life.Data.Div.genes+=('<div class="data">'+Gene.ID+' : '+Gene.Name+'<span><p>HP : '+Gene.HP+' </p><p>Strength : '+Gene.Strength+' </p><p>Sight : '+Gene.Sight+' </p><p>MoveSpeed : '+Gene.MoveSpeed+' </p><p>CollectSpeed : '+Gene.CollectSpeed+' </p><p>ReproductionSpeed : '+Gene.ReproductionSpeed+' </p><p>Aggressivity : '+Gene.Aggressivity+' </p><p>FoodCarry : '+Gene.FoodCarry+' </p><p>FoodConsumption : '+Gene.FoodConsumption+' </p></span></div>');
		
		Life.Data.Genes.push(Gene);
	}
	
	// FOOD
	Life.Data.Food = [];
	var i;
	for(i=0;i<Life.Conf.FoodPoolSize;++i){
		var Food = Object.create(Life.Classes.Food);
		Food.Name=chance.word();
		Food.Color=chance.color({format: 'hex'});
		Food.GenesNutrition=[];
		Life.Data.Div.foods+=('<div class="data" style="background-color:'+Food.Color+'">'+Food.Name+'<span><p><u>Gene nutrition :</u></p>');
		for(j=0;j<Life.Data.Genes.length;++j){
			Food.GenesNutrition.push(YAUF.randomBetweenInt(-10,10));
			Life.Data.Div.foods+='<p>'+Life.Data.Genes[j].Name+' : '+Food.GenesNutrition[Food.GenesNutrition.length-1]+'</p>';
		}
		Life.Data.Div.foods+='</span></div>';
		
		Life.Data.Food.push(Food);
	}
	
	// LIFE FORM
	Life.Data.LifeForms = [];
	var i;
	for(i=0;i<Life.Conf.LifeFormPoolSize;++i){
		var LifeForm = Object.create(Life.Classes.LifeForm);
		LifeForm.ID=i;
		LifeForm.Name=chance.word();
		LifeForm.Color=chance.color({format: 'hex'});
		LifeForm.Status='alive';
		LifeForm.Genes = [];
		LifeForm.HP=0;
		LifeForm.Strength=0;
		LifeForm.Sight=0;
		LifeForm.MoveSpeed=0;
		LifeForm.CollectSpeed=0;
		LifeForm.ReproductionSpeed=0;
		LifeForm.Aggressivity=0;
		LifeForm.FoodCarry=0;
		LifeForm.FoodConsumption=0;
		for(j=0;j<Life.Conf.GenesPerLifeForm;++j){
			var new_gene = Life.Data.Genes[YAUF.randomBetweenInt(0,Life.Data.Genes.length-1)];
			LifeForm.HP+=new_gene.HP;
			LifeForm.Strength+=new_gene.Strength;
			LifeForm.Sight+=new_gene.Sight;
			LifeForm.MoveSpeed+=new_gene.MoveSpeed;
			LifeForm.CollectSpeed+=new_gene.CollectSpeed;
			LifeForm.ReproductionSpeed+=new_gene.ReproductionSpeed;
			LifeForm.Aggressivity+=new_gene.Aggressivity;
			LifeForm.FoodCarry+=new_gene.FoodCarry;
			LifeForm.FoodConsumption+=new_gene.FoodConsumption;
			
			LifeForm.Genes.push(new_gene);
		}
		//LifeForm.Sight=(Math.round(LifeForm.Sight/100)>0?Math.round(LifeForm.Sight/100):1);
		Life.Data.Div.lifeforms+=('<div class="data" style="background-color:'+LifeForm.Color+'">'+LifeForm.ID+' : '+LifeForm.Name+'<span><p>HP : '+LifeForm.HP+' </p><p>Strength : '+LifeForm.Strength+' </p><p>Sight : '+LifeForm.Sight+' </p><p>MoveSpeed : '+LifeForm.MoveSpeed+' </p><p>CollectSpeed : '+LifeForm.CollectSpeed+' </p><p>ReproductionSpeed : '+LifeForm.ReproductionSpeed+' </p><p>Aggressivity : '+LifeForm.Aggressivity+' </p><p>FoodCarry : '+LifeForm.FoodCarry+' </p><p>FoodConsumption : '+LifeForm.FoodConsumption+' </p></span></div>');
		
		Life.Data.LifeForms.push(LifeForm);
	}
})(Life);