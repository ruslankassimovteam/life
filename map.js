/* ===========================================

			Here game data objects placed on map and drawn.

=========================================== */
(function(Life) {
	Life.Map = {
		'Zones':[],
		'Food':[],
		'Lifes':[],
		
		'intersects':function(obj1,obj2) {
			return !( obj1.x >= (obj2.x + obj2.w) || (obj1.x + obj1.w) <=  obj2.x || obj1.y >= (obj2.y + obj2.h) || (obj1.y + obj1.h) <=  obj2.y);
		},
		'generate': function() {
			logs('Creating new map ...',1);
			logs('+ Generating Zones.');
			var i;
			for(i=0;i<Life.Conf.ZonesNb;++i) {	
			
				do {
					var intersects = false;
					
					var zone_nb = YAUF.randomBetweenInt(0,Life.Data.Zones.length-1);
					var potential_zone = {
						'x':YAUF.randomBetweenInt(10,740),
						'y':YAUF.randomBetweenInt(10,740),
						'w':YAUF.randomBetweenInt(10,80),
						'h':YAUF.randomBetweenInt(10,80),
						'data':Life.Data.Zones[zone_nb],
					};
					
					for(j=0;j<this.Zones.length;++j)
					{
						if(this.intersects(potential_zone,this.Zones[j])) {
							intersects=true;
							break;
						}
					}
				}while(intersects);
				
				this.Zones.push(potential_zone);
			}
			
			logs('+ Generating Food.');
			for(i=0;i<Life.Conf.FoodNb;++i) {	
			
				do {
					var intersects = false;
					
					var food_nb = YAUF.randomBetweenInt(0,Life.Data.Food.length-1);
					var potential_food = {
						'x':YAUF.randomBetweenInt(10,740),
						'y':YAUF.randomBetweenInt(10,740),
						'w':YAUF.randomBetweenInt(5,20),
						'h':YAUF.randomBetweenInt(5,20),
						'data':Life.Data.Food[food_nb],
						'qt':YAUF.randomBetweenInt(20,50)
					};
					
					for(j=0;j<this.Zones.length;++j)
					{
						if(this.intersects(potential_food,this.Zones[j])) {
							intersects=true;
							break;
						}
					}
					
					if(!intersects) {
						for(j=0;j<this.Food.length;++j)
						{
							if(this.intersects(potential_food,this.Food[j])) {
								intersects=true;
								break;
							}
						}
					}
				}while(intersects);
				
				this.Food.push(potential_food);
			}
			
			logs('+ Generating Life.');
			for(i=0;i<Life.Conf.LifesNb;++i) {	
			
				do {
					var intersects = false;
					
					var lifeform = YAUF.randomBetweenInt(0,Life.Data.LifeForms.length-1);
					var potential_life = {
						'x':YAUF.randomBetweenInt(10,740),
						'y':YAUF.randomBetweenInt(10,740),
						'w':1,
						'h':1,
						'data':Object.create(Life.Data.LifeForms[lifeform]),
						'last_move' : new Date(),
						'last_collect' : new Date(),
						'last_reproduction' : new Date(),
						'last_consumption' : new Date(),
						'food': Math.round(Life.Data.LifeForms[lifeform].FoodCarry/2),
						'train':true
					};
					
					for(j=0;j<this.Zones.length;++j)
					{
						if(this.intersects(potential_life,this.Zones[j])) {
							intersects=true;
							break;
						}
					}
					
					if(!intersects) {
						for(j=0;j<this.Food.length;++j)
						{
							if(this.intersects(potential_life,this.Food[j])) {
								intersects=true;
								break;
							}
						}
					}
				}while(intersects);
				
				this.Lifes.push(potential_life);
			}

			logs('+ Drawing background.');
			var map = $('#map');
			var map_ctx = map.get(0).getContext('2d');
			// generate background
			map_ctx.fillStyle="#1A5B33";
			map_ctx.fillRect(0,0,750,750);
			
			logs('+ Drawing Zones.');
			for(i=0;i<this.Zones.length;++i) {
				var zone = this.Zones[i];
				map_ctx.fillStyle=zone.data.Color;
				map_ctx.fillRect(zone.x,zone.y,zone.w,zone.h);
			}
			
			// generate world
			var world = $('#world');
			var world_ctx = world.get(0).getContext('2d');
			
			logs('+ Drawing Food.');
			for(i=0;i<this.Food.length;++i) {
				var food = this.Food[i];
				world_ctx.fillStyle=food.data.Color;
				world_ctx.fillRect(food.x,food.y,food.w,food.h);
			}
		}
	}
})(Life);